package fun.lewisdev.deluxehub.inventory;

import org.bukkit.entity.Player;

public interface ClickAction {

    void execute(Player p0);
}
